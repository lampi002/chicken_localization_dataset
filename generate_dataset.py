"""
This script generates images with single instances and corresponding bounding box annotations from a segmentation dataset with coco annotations.
Note: To run all functions, utils from the matterport Mask-R-CNN implementation are required
"""

import os
import sys
import json
import numpy as np
import glob
from PIL import Image, ImageDraw
import pathlib
import cv2  
import csv
import warnings
import scipy
import skimage.transform
import random 

from mrcnn.config import Config
import mrcnn.utils as utils

class Dataset(utils.Dataset):
    """ Generates a COCO-like dataset, i.e. an image dataset annotated in the style of the COCO dataset.
        See http://cocodataset.org/#home for more information.
    """
    def load_data(self, annotation_json, images_dir):
        """ Load the coco-like dataset from json
        Args:
            annotation_json: The path to the coco annotations json file
            images_dir: The directory holding the images referred to by the json file
        """
        # Load json from file
        min_file_size = 10000000
        json_file = open(annotation_json)
        coco_json = json.load(json_file)
        json_file.close()
        
        # Add the class names using the base method from utils.Dataset
        source_name = "coco_like"
        for category in coco_json['categories']:
           class_id = category['id'] 
           class_name = category['name']
           if class_name == "Hen":
              hen_class_id = class_id 
           if class_id < 1:
                print('Error: Class id for "{}" cannot be less than one. (0 is reserved for the background)'.format(class_name))
                return    
           self.add_class(source_name, class_id, class_name)
         
        classes = []     # to have a list which just contains the class names and the number of instances for each 
        for element in self.class_info:
           class_entry = {
                         'name' : element['name'],
                         'count' : 0
                        }
           classes.append(class_entry)

        # Get annotations:
        annotations = {}
        for annotation in coco_json['annotations']:
            image_id = annotation['image_id']
            if image_id not in annotations:
               annotations[image_id] = []
            if annotation['category_id'] == hen_class_id-1:
               annotations[image_id].append(annotation)
        
        # Get all images and add them to the dataset
        seen_images = {}
        for image in coco_json['images']:
            image_id = image['id']
            if image_id in seen_images:
                print("Warning: Skipping duplicate image id: {}".format(image))
            else:
                seen_images[image_id] = image
                try:
                   #image_file_name = format(image['id'], '08d')+"_"+image['file_name']
                    image_file_name = image['file_name'] 
                    print(image_file_name)
                    image_width = image['width']
                    image_height = image['height']
                except KeyError as key:
                    print("Warning: Skipping image (id: {}) with missing key: {}".format(image_id, key))

                find_file = glob.glob(images_dir + "/**/" + image_file_name, recursive = True) # search in all subdire$
                if len(find_file) >0:
                   image_path = os.path.abspath(os.path.join(find_file[0]))               
                else:
                   image_path = "not_found"
                image_annotations = annotations[image_id]
                hasCrowd = False # Quick fix to avoid problems with "count" annotations. If more than 1segmentation/object -> skip image
                for element in image_annotations:
                  if element['iscrowd']:
                    hasCrowd = True 
                
                # Add the image using the base method from utils.Dataset (if file exists)
                if os.path.isfile(image_path):
                 if not hasCrowd:  # to avoid that images of object with "multiple parts" are added
                  for annotation in image_annotations:
                     id = (annotation['category_id'])+1
                     classes[id]['count'] +=1
                  self.add_image(
                      source=source_name,
                      image_id=image_id,
                      path=image_path,
                      width=image_width,
                      height=image_height,
                      annotations=image_annotations)

        print("...........Dataset-Info:.......................\n")
        for i in range(len(classes)):
           print ("Class: {}, Instances: {}".format(classes[i]['name'], classes[i]['count']))

    def load_mask(self, image_id):
        """ Load instance masks for the given image.
        MaskRCNN expects masks in the form of a bitmap [height, width, instances].
        Args:
            image_id: The id of the image to load masks for
        Returns:
            masks: A bool array of shape [height, width, instance count] with
                one mask per instance.
            class_ids: a 1D array of class IDs of the instance masks.
        """
        
        image_info = self.image_info[image_id]
        annotations = image_info['annotations']
        instance_masks = []
        class_ids = []
        
        for annotation in annotations:
            class_id = annotation['category_id']
            mask = Image.new('1', (image_info['width'], image_info['height']))
            mask_draw = ImageDraw.ImageDraw(mask, '1')
            for segmentation in annotation['segmentation']:
                mask_draw.polygon(segmentation, fill=1)
                bool_array = np.array(mask) > 0
                instance_masks.append(bool_array)
                class_ids.append(class_id)

        image = self.load_image(image_id, use_depth=False)
        mask = np.dstack(instance_masks)
        class_ids = np.array(class_ids, dtype=np.int32)
              
        return mask, class_ids

class ProcessDataset():
     def __init__(self,dataset):
       self.dataset = dataset
       self.cfg=Config

     def generate_images(self, maximum_size=None, object_scaling_factor=3, background_directory="/media/christian/SamsungSSD/deep_learning_course/chicken_localization_dataset/backgrounds"):
       """
       Segments instances from an image and places each in a new image in front of a randomly chosen 
       background from a folder. If background_directory="None", bg will be black.
       
       masks: [height, width, num_instances]
       maximum_size: defines the maximum number of images in this dataset
       background_directory: path of folder that contains background images 
       """
               
       # check first image if train or val:
       global_path = pathlib.PurePath(self.dataset.image_info[0]['path'])
       path_split =  global_path.parts
       directory = global_path.parents[2]
       parentfolder = path_split[-3] #train/val
       
       # create new directory for rgb images 
       new_rgb_directory = 'images/'+ parentfolder + "/"
       #new_rgb_directory = 'images/'+ parentfolder + "/" 
       pathlib.Path(new_rgb_directory).mkdir(parents=True, exist_ok=True)
       
       annotations = []  
       image_count = 0
       for index,image_id in enumerate(self.dataset.image_ids):
         print("Processing image:id ",image_id) 
         
         # load image, bounding boxes and masks for the image id
         image_info = self.dataset.image_info[image_id]
         image_original = dataset.load_image(image_id, use_depth=False)
         masks_original, class_ids = dataset.load_mask(image_id)
         
         # resize image and mask (mask is scaled down from 2208x1920, image from 384x384)
         new_size = 256
         image, _, _, _, _ = resize_image(image_original, [image_original.shape[0], image_original.shape[1]], min_dim=None, min_scale=None, max_dim=new_size)         
         _, window, scale, padding, crop = resize_image(image_original, [image_info['width'], image_info['height']], min_dim=None, min_scale=None, max_dim=new_size)
         masks = resize_mask(masks_original, scale, padding, crop)
         boxes = utils.extract_bboxes(masks)
         boxes = box_corner_to_center(boxes)
         
         #segment instances and place each in front of an empty background:
         N = masks.shape[-1]
         
         if N <= 3: 
            for i in range(N):
                random_file = random.choice(os.listdir(background_directory))
                background_image = skimage.io.imread(background_directory + "/" + random_file)
                # If grayscale. Convert to RGB for consistency.
                if background_image.ndim != 3:
                   background_image = skimage.color.gray2rgb(background_image)
                # If has an alpha channel, remove it for consistency
                if background_image.shape[-1] == 4:
                   background_image=background_image[..., :3]
                bg_w, bg_h = background_image.shape[:2]
                resized_background = resize(background_image, (round(bg_h * 0.3), round(bg_w * 0.3)), preserve_range=True) # reduce background size
                resized_background = resize_image(resized_background, [bg_w, bg_h], max_dim=image.shape[0], min_dim=image.shape[0], mode="crop")[0] #crop background
                mask = masks[:, :, i]
                # double the object size 
                #mask_original = masks_original[:, :, i]
                #image, mask, boxes = resize_object(object_scaling_factor, image, image_original, mask_original)
                class_id = class_ids[i]
                x, y, w, h = boxes[i]
                for c in range(image.shape[2]):
                     resized_background[:, :, c] = np.where(mask == 1,
                                          image[:, :, c],
                                          resized_background[:, :, c])
                augmented_image = resized_background        
                augmented_image = cv2.cvtColor(augmented_image, cv2.COLOR_RGB2BGR)
                #augmented_image = cv2.rectangle(augmented_image, (int(x-w/2), int(y-h/2)), (int(x+w/2), int(y+h/2)), (0, 255, 0), 2)
                savepath = str(new_rgb_directory)+str(image_id) + "_" + str(i) +".png"  
                
                label = [savepath, class_id, x, y, w, h]
                annotations.append(label)
                print("Writing modified file to: ", savepath)
                cv2.imwrite(savepath, augmented_image)
                image_count +=1
                
            if maximum_size!=None:
                print("maximum and actual:" , maximum_size, image_count)
                if image_count > maximum_size:
                    break
         
         
         
         #create .csv annotation file with [img_name, label, x, y, w, h]
         
         headers = ['img_name', 'label', 'x', 'y', 'w', 'h']
         with open('annotations_' + parentfolder +'.csv', 'w', encoding='UTF8', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(headers)
            writer.writerows(annotations)
         

       return 0 

def resize_object(scaling_factor, image, image_original, mask):
    """
    A function that resizes an object by the given size and returns the new mask and bounding boxes. Only implemented for squared images so far!
    """    
    
    mask = np.expand_dims(mask, axis=2)
    # resize original image to desired format 
    img_size_scaled = scaling_factor * image.shape[0]
    original_image_resized, _, _,_, _ = resize_image(image_original, [image_original.shape[1], image_original.shape[0]], min_dim=img_size_scaled, min_scale=None, max_dim=img_size_scaled)
    # resize mask to desired scaled format: 
    _, window, scale, padding, crop = resize_image(mask, [mask.shape[1], mask.shape[0]], min_dim=None, min_scale=None, max_dim=img_size_scaled)
    mask = resize_mask(mask, scale, padding, crop)
    boxes = utils.extract_bboxes(mask)
    boxes = box_corner_to_center(boxes)
    x, y, w, h = boxes[0] # center coordinates + h + w 
    
    #segment the object -  crop original image and mask so that the object is in centered and crop has size of image. Compute new bounding boxes
    crop_w = image.shape[1]
    crop_h = image.shape[0]
    
    # compute left corner(x1,y1), width and height: 
    crop_x1 = x - image.shape[1]/2
    if crop_x1 < 0: 
        crop_x1 = 0 
    if crop_x1 + crop_w > original_image_resized.shape[1]:
       crop_x1 = original_image_resized.shape[1] - crop_w
        
    crop_y1 = y - image.shape[0]/2
    if crop_y1 < 0: 
        crop_y1 = 0 
    if crop_y1 + crop_h > original_image_resized.shape[0]:
       crop_y1 = original_image_resized.shape[0] - crop_h
    
    crop_x1 = int(crop_x1)
    crop_y1 = int(crop_y1)
    
    image_cropped = original_image_resized[crop_y1:crop_y1 + crop_h, crop_x1:crop_x1 + crop_w]
    mask = mask[crop_y1:crop_y1 + crop_h, crop_x1:crop_x1 + crop_w]
    boxes = utils.extract_bboxes(mask)
    boxes = box_corner_to_center(boxes)
    print("new box", boxes)
    mask = np.squeeze(mask) 
    #If the mask is scaled down, we do not center it on the object, as the object can never be outside of the old image boundaries (size is smaller than before). 
    
    return image_cropped, mask, boxes
    

def resize(image, output_shape, order=1, mode='constant', cval=0, clip=True,
           preserve_range=False, anti_aliasing=False, anti_aliasing_sigma=None):
    """A wrapper for Scikit-Image resize().
    Scikit-Image generates warnings on every call to resize() if it doesn't
    receive the right parameters. The right parameters depend on the version
    of skimage. This solves the problem by using different parameters per
    version. And it provides a central place to control resizing defaults.
    """
    
    return skimage.transform.resize(
        image, output_shape,
        order=order, mode=mode, cval=cval, clip=clip,
        preserve_range=preserve_range, anti_aliasing=anti_aliasing,
        anti_aliasing_sigma=anti_aliasing_sigma)
    

def resize_image(image, original_shape, min_dim=None, max_dim=None, min_scale=None, mode="square"):
    """Resizes an image keeping the aspect ratio unchanged.
    min_dim: if provided, resizes the image such that it's smaller
        dimension == min_dim
    max_dim: if provided, ensures that the image longest side doesn't
        exceed this value.
    min_scale: if provided, ensure that the image is scaled up by at least
        this percent even if min_dim doesn't require it.
    mode: Resizing mode.
        none: No resizing. Return the image unchanged.
        square: Resize and pad with zeros to get a square image
            of size [max_dim, max_dim].
        pad64: Pads width and height with zeros to make them multiples of 64.
               If min_dim or min_scale are provided, it scales the image up
               before padding. max_dim is ignored in this mode.
               The multiple of 64 is needed to ensure smooth scaling of feature
               maps up and down the 6 levels of the FPN pyramid (2**6=64).
        crop: Picks random crops from the image. First, scales the image based
              on min_dim and min_scale, then picks a random crop of
              size min_dim x min_dim. Can be used in training only.
              max_dim is not used in this mode.
    Returns:
    image: the resized image
    window: (y1, x1, y2, x2). If max_dim is provided, padding might
        be inserted in the returned image. If so, this window is the
        coordinates of the image part of the full image (excluding
        the padding). The x2, y2 pixels are not included.
    scale: The scale factor used to resize the image
    padding: Padding added to the image [(top, bottom), (left, right), (0, 0)]
    """
    # Keep track of image dtype and return results in the same dtype
    image_dtype = np.uint8
    # Default window (y1, x1, y2, x2) and default scale == 1.
    w, h = original_shape
    window = (0, 0, h, w)
    scale = 1
    padding = [(0, 0), (0, 0), (0, 0)]
    crop = None

    if mode == "none":
        return image, window, scale, padding, crop

    # Scale?
    if min_dim:
        # Scale up but not down
        scale = max(1, min_dim / min(h, w))
    if min_scale and scale < min_scale:
        scale = min_scale

    # Does it exceed max dim?
    if max_dim and mode == "square":
        image_max = max(h, w)
        if round(image_max * scale) > max_dim:
            scale = max_dim / image_max

    # Resize image using bilinear interpolation
    if scale != 1:
        image = resize(image, (round(h * scale), round(w * scale)),
                       preserve_range=True)

    # Need padding or cropping?
    if mode == "square":
        # Get new height and width
        h, w = image.shape[:2]
        top_pad = (max_dim - h) // 2
        bottom_pad = max_dim - h - top_pad
        left_pad = (max_dim - w) // 2
        right_pad = max_dim - w - left_pad
        padding = [(top_pad, bottom_pad), (left_pad, right_pad), (0, 0)]
        image = np.pad(image, padding, mode='constant', constant_values=0)
        window = (top_pad, left_pad, h + top_pad, w + left_pad)
    elif mode == "pad64":
        h, w = image.shape[:2]
        # Both sides must be divisible by 64
        assert min_dim % 64 == 0, "Minimum dimension must be a multiple of 64"
        # Height
        if h % 64 > 0:
            max_h = h - (h % 64) + 64
            top_pad = (max_h - h) // 2
            bottom_pad = max_h - h - top_pad
        else:
            top_pad = bottom_pad = 0
        # Width
        if w % 64 > 0:
            max_w = w - (w % 64) + 64
            left_pad = (max_w - w) // 2
            right_pad = max_w - w - left_pad
        else:
            left_pad = right_pad = 0
        padding = [(top_pad, bottom_pad), (left_pad, right_pad), (0, 0)]
        image = np.pad(image, padding, mode='constant', constant_values=0)
        window = (top_pad, left_pad, h + top_pad, w + left_pad)
    elif mode == "crop":
        # Pick a random crop
        h, w = image.shape[:2]
        y = random.randint(0, (h - min_dim))
        x = random.randint(0, (w - min_dim))
        crop = (y, x, min_dim, min_dim)
        image = image[y:y + min_dim, x:x + min_dim]
        window = (0, 0, min_dim, min_dim)
    else:
        raise Exception("Mode {} not supported".format(mode))
    return image.astype(image_dtype), window, scale, padding, crop

  
def resize_mask(mask, scale, padding, crop=None):
    """Resizes a mask using the given scale and padding.
    Typically, you get the scale and padding from resize_image() to
    ensure both, the image and the mask, are resized consistently.
    scale: mask scaling factor
    padding: Padding to add to the mask in the form
            [(top, bottom), (left, right), (0, 0)]
    """
    # Suppress warning from scipy 0.13.0, the output shape of zoom() is
    # calculated with round() instead of int()
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        mask = scipy.ndimage.zoom(mask, zoom=[scale, scale, 1], order=0)
    if crop is not None:
        y, x, h, w = crop
        mask = mask[y:y + h, x:x + w]
    else:
        mask = np.pad(mask, padding, mode='constant', constant_values=0)
    return mask

def box_corner_to_center(boxes):
    """
    Convert from (upper-left, lower-right) to (center, width, height).
    """
    y1,x1,y2,x2 = boxes[:, 0], boxes[:, 1], boxes[:, 2], boxes[:, 3]
    x = (x1 + x2) / 2
    y = (y1 + y2) / 2
    w = x2 - x1
    h = y2 - y1
    boxes = np.stack((x, y, w, h), axis=-1)
    return boxes

    

dataset = Dataset()
dataset.load_data('/media/christian/SamsungSSD/deep_learning_course/chicken_segmentation_dataset/annotations.json', '/media/christian/SamsungSSD/deep_learning_course/chicken_segmentation_dataset/train/')
dataset.prepare()
processed_data = ProcessDataset(dataset)
processed_data.generate_images()
