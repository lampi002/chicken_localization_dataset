# chicken_localization_dataset

This repo includes RGB-images of single chickens and the corresponding bounding box annotations for each image. 
All images in the train and test folders are padded and resized to a resolution of 256 x 256 pixels. The folder rain_subset contains a subset of the training images resized to 128x128 with enlarged chickens. 
The images in this dataset were generated artificially and can be reproduced using generate_dataset.py

